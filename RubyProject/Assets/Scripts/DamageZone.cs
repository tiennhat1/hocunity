﻿using UnityEngine;

/// <summary>
/// xử lý khi nhân vật đi vào chông
/// </summary>
public class DamageZone : MonoBehaviour 
{
    void OnTriggerStay2D(Collider2D other)
    {
        RubyController controller = other.GetComponent<RubyController>();

        if (controller != null)
        { 
            controller.ChangeHealth(-1);
        }
    }
}
