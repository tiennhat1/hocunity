﻿using System;
using UnityEngine;
 
public class Enemy : MonoBehaviour
{
	 //tốc đọ của enemy
	public float speed;
	//thời gian thay đổi hướng của enemy
	public float timeToChange;
	//xet hướng di chuyển của quái
	public bool horizontal;
	//hiệu ứng khói của quái
	public GameObject smokeParticleEffect;
	public ParticleSystem fixedParticleEffect;
	 
	
	Rigidbody2D rigidbody2d;
	float remainingTimeToChange;
	Vector2 direction = Vector2.right;
	bool repaired = false;
	 
	
	void Start ()
	{
		rigidbody2d = GetComponent<Rigidbody2D>();
		remainingTimeToChange = timeToChange;

		direction = horizontal ? Vector2.right : Vector2.down; 
	}
	
	void Update()
	{
		if(repaired)
			return;
		//hết thời gian đêm ngược thì hướng của enemy sẽ bị thay đổi
		remainingTimeToChange -= Time.deltaTime;

		if (remainingTimeToChange <= 0)
		{
			remainingTimeToChange += timeToChange;
			direction *= -1;
		}
		 
	}

	void FixedUpdate()
	{
		rigidbody2d.MovePosition(rigidbody2d.position + direction * speed * Time.deltaTime);
	}

	void OnCollisionStay2D(Collision2D other)
	{
		if(repaired)
			return;
		
		RubyController controller = other.collider.GetComponent<RubyController>();
		
		if(controller != null)
			controller.ChangeHealth(-1);
	}
	/// <summary>
	/// khi bị bắn trúng
	/// </summary>
	public void Fix()
	{ 
		repaired = true;
		
		smokeParticleEffect.SetActive(false);
		//khởi tạo partical khi đứng yên
		Instantiate(fixedParticleEffect, transform.position + Vector3.up * 0.5f, Quaternion.identity);

		 
		rigidbody2d.simulated = false; 
	}
}
