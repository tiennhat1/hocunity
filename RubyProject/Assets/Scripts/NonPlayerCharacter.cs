﻿using UnityEngine;

 /// <summary>
 /// xử lý hiển thị dialog bõ khi nhân vật ấn phím X và đứng trước ếch
 /// </summary>
public class NonPlayerCharacter : MonoBehaviour
{
    //thời gian hiển thị dialog box
    public float displayTime = 4.0f;
    //gameObject dialog box
    public GameObject dialogBox;
    //thời gian đêm ngươc
    float timerDisplay;
    
    void Start()
    {
        dialogBox.SetActive(false); 
    }

    void Update()
    {
        //điếm ngược thời gian hiển thi dialog box
        if (timerDisplay >= 0)
        {
            timerDisplay -= Time.deltaTime;
            if (timerDisplay < 0)
            {
                dialogBox.SetActive(false);
            }
        }
    }
    /// <summary>
    /// xử lí hiển thị dialog box
    /// </summary>
    public void DisplayDialog()
    {
        timerDisplay = displayTime;
        dialogBox.SetActive(true);
    }
}
