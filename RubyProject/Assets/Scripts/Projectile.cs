﻿using UnityEngine;
 
public class Projectile : MonoBehaviour
{
    Rigidbody2D rigidbody2d;
    
    void Awake()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    { 
        //nếu khoảng cách lớn hơn 1000 thì phá hủy đạn
        if(transform.position.magnitude > 1000.0f)
            Destroy(gameObject);
    }
     /// <summary>
     /// xử lý di chuyển của đạn
     /// </summary>
     /// <param name="direction">hướng di chuyển</param>
     /// <param name="force">lực</param>
    public void Launch(Vector2 direction, float force)
    {
        rigidbody2d.AddForce(direction * force);
    }
    /// <summary>
    /// xử ký va chạm với đạn
    /// </summary>
    /// <param name="other"></param>
    void OnCollisionEnter2D(Collision2D other)
    {
        Enemy enemy = other.collider.GetComponent<Enemy>();
         
        if (enemy != null)
        {
            enemy.Fix();
        }
        
        Destroy(gameObject);
    }
}
