﻿using System;
using UnityEngine;
/// <summary>
/// điều khiển nhân vật
/// </summary>
public class RubyController : MonoBehaviour
{ 
    //tốc độ di chuyển của nhân vật
    public float speed = 4;
     //máu tối đa 
    public int maxHealth = 5;
    //thời gian mà nhân vật k bị trừ máu sau khi va chạm
    public float timeInvincible = 2.0f;
    //vị trí hồi sinh nhan vật
    public Transform respawnPosition;
    //hiệu ứng khi va chạm với quái hoặc chông
    public ParticleSystem hitParticle;
    
    //đạn
    public GameObject projectilePrefab; 
     
    //số máu 
    public int health
    {
        get { return currentHealth; }
    }
     
    Rigidbody2D rigidbody2d;
    //giá trị điều khiển
    Vector2 currentInput;
     //số máu hiện tại
    int currentHealth;
    //thời gian còn lại sau khi va chạm
    float invincibleTimer;
    //set xem có bất khả chiến bại
    bool isInvincible; 
    //hướng khi chuyển động
    Vector2 lookDirection = new Vector2(1, 0); 
    
    void Start()
    { 
        rigidbody2d = GetComponent<Rigidbody2D>(); 
        currentHealth = maxHealth; 
    }

    void Update()
    {
        //nếu đang bất khả chiến bại thì biến invincibleTimer sẽ trừ dần đến khi nhỏ hơn 0 thì biến isInvincible sẽ trả về false
        if (isInvincible)
        {
            Debug.Log($"co bi tru mau k::{isInvincible}");
            invincibleTimer -= Time.deltaTime;
            Debug.Log($"timer:{invincibleTimer}");
            if (invincibleTimer < 0)
                isInvincible = false;
           
        }
         
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Debug.Log($"horizontal::{horizontal}===vertical::{vertical}");        
        Vector2 move = new Vector2(horizontal, vertical);
          
        currentInput = move;  
    }

    void FixedUpdate()
    {
        Vector2 position = rigidbody2d.position;
        
        position = position + currentInput * speed * Time.fixedDeltaTime;
        
        rigidbody2d.MovePosition(position);
        Debug.Log($"so mau hien tai::: {currentHealth}");
    }
     
    public void ChangeHealth(int amount)
    {
        //nếu bị va cham với quái hoặc bẫy
        if (amount < 0)
        { 
            //nếu trong thời gian k bị trừ máu thì thời gian set bất khả chiến bại sẽ bằng với thời gian điếm ngược
            if (isInvincible)
                return;
            
            isInvincible = true;
            invincibleTimer = timeInvincible;
             
            //khởi tạo partical lúc va chạm có chiều hướng lên trên
            Instantiate(hitParticle, transform.position + Vector3.up * 0.5f, Quaternion.identity);
        } 
        //máu hiện tại của nhân vật thuộc khoảng từ 0 đến maxhealth
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);

        if (currentHealth == 0)
            Respawn(); 
    }

    void Respawn()
    {
        ChangeHealth(maxHealth);
        transform.position = respawnPosition.position;
    }
         
}
